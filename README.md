# forge-minecraft

## Description
This repository contains the modpack for our Minecraft server. Each addition/upgrade to the modpack is captured as a Release, so you can see what changes between versions.

## Installation
Copy the latest Release .zip folder to your Minecraft mods folder.

## Changelog

### 2025-02-08

Lots more mods for structures and neat things.

 * [Alex's Mobs](https://www.curseforge.com/minecraft/mc-mods/alexs-mobs) - 1.18.6.
   * Alex's Mobs adds over 90 new mobs, mostly real animals with a few fantastic ones.
 * [Aquaculture 2](https://www.curseforge.com/minecraft/mc-mods/aquaculture) - 2.3.12.
   * Aquaculture adds fish and fishing enhancements, which I'm excited for since fishing has been integral to our survival so far. Lots of new fish species and rods and things like that.
 * [Biomes O' Plenty](https://www.curseforge.com/minecraft/mc-mods/biomes-o-plenty) - 16.0.0.109
   * Adds many biomes and world environments to world-gen, tons of new block types, structures, etc.
 * [Dungeon Crawl](https://www.curseforge.com/minecraft/mc-mods/dungeon-crawl) - 2.3.14.
   * LOoks like it could be fun, spawns aboveground entrances to dungeons below. More exploration is always good!
 * [Goblins and Dungons](https://www.curseforge.com/minecraft/mc-mods/goblins-dungeons) - 1.0.8.
   * Adds several mobs, including goblins, and a few new structures where said mobs would live.
 * [Goblin Traders](https://www.curseforge.com/minecraft/mc-mods/goblin-traders) - 1.8.0.
   * Adds goblins to trade with underground and in the nether, similar to Wandering Traders.
 * [Guard Villagers](https://www.curseforge.com/minecraft/mc-mods/guard-villagers) - 1.4.4.
   * Adds guards to each village to help protect them, so it doesn't fall completely on the Iron Golems.
 * [It Takes a Pillage](https://www.curseforge.com/minecraft/mc-mods/it-takes-a-pillage) - 1.0.3.
   * Full-on expansion of Illagers with their own camps and fortresses for us to defeat.
 * [Macaw's Bridges](https://www.curseforge.com/minecraft/mc-mods/macaws-bridges) - 3.0.1.
   * Adds fancy bridge-building blocks. They may be natuarally occuring as well?
 * [More Villagers](https://www.curseforge.com/minecraft/mc-mods/more-villagers) - 3.3.2.
   * Adds several more Villager occupations and trade possibilities!
 * [Mowzie's Mobs](https://www.curseforge.com/minecraft/mc-mods/mowzies-mobs) - 1.6.3.
   * Adds many new mobs, mostly unfriendly, to defeat.
 * [Pam's HarvestCraft 2 - Crops](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-crops) - 1.0.6.
 * [Pam's HarvestCraft 2 - Food Core](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-core) - 1.0.3.
 * [Pam's HarvestCraft 2 - Food Expanded](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-extended) - 1.0.5.
 * [Pam's HarvestCraft 2 - Trees](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-trees) - 1.0.4.
   * HarvestCraft mods add a bunch of additions to farming and food. It's a classic that I've never done much with, so I'm excited to try it out.
 * [Potion Descriptions](https://www.curseforge.com/minecraft/mc-mods/potion-descriptions) - 1.7.
   * Adds tooltips to explain what potions do.
 * [Serene Seasons](https://www.curseforge.com/minecraft/mc-mods/serene-seasons) - 7.0.0.13.
   * Adds seasons to Minecraft. I'm very curious to see how long a "year" is, and how long it takes to cycle through a full season.
 * [The One Probe](https://www.curseforge.com/minecraft/mc-mods/the-one-probe) - 5.1.2.
   * Adds a "more-immersive" way of seeing item information in-game.
   * Apparently I never actually added this previously...
 * [Towns and Towers](https://www.curseforge.com/minecraft/mc-mods/towns-and-towers) - 1.10.0.1.
   * Village expansion, adds many new structures. Mostly compatible, Waystones will not be added to these new villages by default, at least according to patch notes. I guess we'll see if that has been fixed by now.
 * [YUNG's API](https://www.curseforge.com/minecraft/mc-mods/yungs-api) - 2.2.9.
 * [YUNG's Better Dungeons](https://www.curseforge.com/minecraft/mc-mods/yungs-better-dungeons) - 2.1.0.
 * [YUNG's Better Mineshafts](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge) - 2.2.
 * [YUNG's Better Strongholds](https://www.curseforge.com/minecraft/mc-mods/yungs-better-strongholds) - 2.1.1.
 * [YUNG's Bridges](https://www.curseforge.com/minecraft/mc-mods/yungs-bridges) - 2.1.0.
 * [YUNG's Extras](https://www.curseforge.com/minecraft/mc-mods/yungs-extras) - 2.1.0.
   * YUNG mods add tons of reworks for various structures, as seen in the mod descriptions.

### 2025-02-02
Adding one mod for QOL.

 * [Death Chest](https://www.curseforge.com/minecraft/mc-mods/death-chest) - 1.6.2a

### 2025-01-26
Adding some new mods!
 * [Enchantment Descriptions](https://www.curseforge.com/minecraft/mc-mods/enchantment-descriptions) - 10.0.12.
   * Quality-of-life mod that helps make enchantments easier to understand.
 * [Ice and Fire: Dragons](https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons) - 2.1.12
   * Perhaps doesn't belong, looks more of a overhaul than an additions mod. It adds it's own home screen! Definitely want to check it out, but I wouldn't be too sad if it were removed. I'm sure there are other mods that allow draconic creatures.
 * [Iron's Spells 'n Spellbooks](https://www.curseforge.com/minecraft/mc-mods/irons-spells-n-spellbooks) - 1.1.4.5
   * Adds a bunch of magical items and monsters.
 * [Quark](https://www.curseforge.com/minecraft/mc-mods/quark) - 3.2-358.
   * Add a bunch of little changes that I want to check out.
 * [Similsax Transtructors - Builder's Wands](https://www.curseforge.com/minecraft/mc-mods/similsax-transtructors) - 1.0.22.
   * Quality-of-life mod that adds a builder's wand to allow for easier horizontal or vertical building.
 * [Waystones](https://www.curseforge.com/minecraft/mc-mods/waystones) - 10.2.2.
   * Adds buildable teleportation waystones, so you don't necessarily need to rely on long minecart networks.

Mod Libraries
 * [AutoRegLib](https://www.curseforge.com/minecraft/mc-mods/autoreglib) - 1.7-53
    * Required for Quark.
 * [Balm](https://www.curseforge.com/minecraft/mc-mods/balm) - 3.2.6.
    * Required for Waystones.
 * [Bookshelf](https://www.curseforge.com/minecraft/mc-mods/bookshelf) - 13.3.56
    * Required for Enchantment Descriptions
 * [Caelus](https://www.curseforge.com/minecraft/mc-mods/caelus) - 3.0.0.2.
    * Required for Iron's Spells 'n Spellbooks.
 * [Citadel](https://www.curseforge.com/minecraft/mc-mods/citadel) - 1.11.3.
    * Required for Ice and Fire: Dragons and Alex's Mobs.
 * [GeckoLib](https://www.curseforge.com/minecraft/mc-mods/geckolib) 3.0.57.
    * Required for Iron's Spells 'n Spellbooks.
 * [Curios](https://www.curseforge.com/minecraft/mc-mods/curios) - 5.0.9.2.
    * Required for Iron's Spells 'n Spellbooks.
 * [playerAnimator](https://www.curseforge.com/minecraft/mc-mods/playeranimator) - 1.0.2.
    * Required for Iron's Spells 'n Spellbooks.
 * [Shutup Experimental Settings!](https://www.curseforge.com/minecraft/mc-mods/shutup-experimental-settings) - 1.0.5
    * Optional requirment for Quark.
 * [TerraBlender](https://www.curseforge.com/minecraft/mc-mods/terrablender) - 1.2.0.126.
    * Optional requirment for Quark.

Updating some mods!
 * [Forge](https://files.minecraftforge.net/net/minecraftforge/forge/index_1.18.2.html) was updated from 40.3.0 to 40.3.3.
 * [PackedUp](https://www.curseforge.com/minecraft/mc-mods/packed-up-backpacks) is updated from 1.0.30 to 1.1.0.
 * [Xaero's Minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap) is updated from 24.7.1 to 25.0.

### 2024-12-31
Happy New Year! I'm revisting this since my son wants to play some Minecraft. Tinker's Construct has been updated for 1.19.2, but I think we'll stick with 1.18.2 for the time being. Here's the differences.

 * Forge was updated to 40.3.0 from 40.2.0.
 * Chunkloaders remains 1.2.8a.
 * PackedUp remains 1.0.30.
 * StorageDrawers remains 10.2.1.
 * Tinker's Construct is updated from 3.6.4.113 to 3.7.2.167.
 * Xaero's Minimap is updated from 23.8.4 to 24.7.1.
 * Xaero's World Map is updated from 1.36.0 to 1.39.2.
 * Mantle is updated from 1.9.45 to 1.9.54.
 * SuperMartijn642 Config Lib remains 1.1.8.
 * SuperMartijn642 Core Lib is updated from 1.1.15 to 1.1.18.